import numpy as np
import pandas as pd
from pandas import ExcelWriter
import argparse
import glob
import os
import openpyxl
from openpyxl.utils import get_column_letter
from openpyxl.styles import Font, Alignment, Border, Side
from openpyxl.styles.fills import PatternFill
from openpyxl.styles.colors import Color
import datetime
from shutil import copyfile

def consolidate_csvs(folderpath):

    df = pd.DataFrame()
    print("Files in {}:".format(folderpath))
    for f in glob.glob(folderpath+"/*.csv"):
        print("appending: {}".format(f))
        df_temp = pd.read_csv(f)

        df = df.append(df_temp)
    
    print("Consolidated DataFrame Info: ")
    print(df.info())

    return df


def getdatestring_from_filename(x): 
    try: 
        stringarray = x.split("_")
        datestring = datetime.datetime.strptime(stringarray[2], '%Y%m%d%H%M%S') 
    except: 
        datestring = np.NaN 
     
    return datestring


def save_xls(df, list_dfs, xls_path):

    # ===== compute excel results ===== 
    with ExcelWriter(xls_path) as writer:
        for sheetname in list_dfs:
            xls = df[df["Rotameter Tag"]== sheetname]


            result = (xls.groupby(["date", "hourmin", 
                                  #"Rotameter Tag",
                                  "Excel_Num", "Excel_Tag"])["bob_value"] 
                        .mean()
                        .round(2).reset_index() 
                        .pivot_table(
                            index=["date", "hourmin"],  
                            columns=[#"Rotameter Tag", 
                                "Excel_Num", "Excel_Tag"
                                    ],  
                            values="bob_value")
                        #.reset_index()
                        )
            result.to_excel(writer, sheet_name=sheetname)
        # save excel file
        writer.save()
    
    # ===== manipulate formats ===== 
    
    #1. read excel file
    workbook = openpyxl.load_workbook(xls_path)
    
    colorpalette = ["ffff00", "0094ff", "aaf7fd", 
                    "00a8f2", "ff99dd", "ff668e", 
                    "fcf7a6", "b9b9ac", "dddbd5"]
    # 2. for each sheet, do
    for i, sheetname in enumerate(list_dfs):
        sht = workbook[sheetname]
        
        # 2a. get max column num and letter
        lastcolumn = sht.max_column
        lastrow = sht.max_row
        lastcolumnletter = get_column_letter(lastcolumn)
        
        # 2b. unbold all
        for row in sht["A1:"+lastcolumnletter+str(lastrow)]:
            for cell in row:
                cell.font = Font(bold=False)
                
        # 2c. shift cells down
        sht.move_range("C1:"+lastcolumnletter+"2", rows=1, cols=0)
        
        # 2d. merge A1:B2 (Rotameter Tag)
        sht.merge_cells("A1:B2")
        sht["A1"].value = "Rotameter Tag"
        sht["A1"].alignment = Alignment(horizontal='center', vertical="center")
        
        # 2e. merge top row for banner
        sht.merge_cells("C1:"+lastcolumnletter+"1")
        sht["C1"].value = sheetname
        sht["C1"].alignment = Alignment(horizontal='center', vertical="center")
        sht["C1"].font = Font(bold=True)
        sht["C1"].fill = PatternFill(patternType='solid', 
                                fgColor=Color(rgb=colorpalette[i%len(colorpalette)])
                                                  )
        # 2f. set Date, Time (Hrs) value
        sht["A3"].value = "Date"
        sht["B3"].value = "Time (Hrs)"
        
        # 2g. bold the numerical headers
        for row in sht["C2:"+lastcolumnletter+"2"]:
            for cell in row:
                cell.font = Font(bold=True)

        for row in sht[lastcolumnletter+"1:"+lastcolumnletter+str(lastrow)]:
            for c in row:
                c.border = Border(right=Side(style="thin"))
        
        for row in sht["C2:"+lastcolumnletter+"3"]:
            for c in row:
                c.border = Border(top=Side(style="thin"),left=Side(style="thin"),
                                  right=Side(style="thin"), bottom=Side(style="thin"))
        # 2i. set tab color
        sht.sheet_properties.tabColor = colorpalette[i%len(colorpalette)]
        
    # 3. save workbook
    workbook.save(xls_path)


def outputpivottable(df, outputfilepath, mapping): 

    # 1. drop all null values
    df.dropna(inplace=True)
    # 2. drop unknown Bob Values, 0s and Inf
    #df = df[df.bob_value.str.isnumeric()]
    df = df[(df.bob_value != 0
        ) & (df.bob_value !="unknown"
        ) & (df.bob_value!="inf")]

    # 3. create new datetime column 
    df['datetime'] = df['imagename'].apply(lambda x: getdatestring_from_filename(x)) 
    
    print(df.head(3))
    # 4. drop zero date files.
    df = df[df["datetime"].notna()]  
    
    # 5. create micro date columns
    df["date"] = df["datetime"].dt.date 
    df["hourmin"] = df["datetime"].dt.floor('30min').dt.strftime('%H%M').apply(str) 
    
    # 6. coerce bob_value to numeric
    df["bob_value"] = pd.to_numeric(df["bob_value"])
    
    # 7. merge with mapping
    df_new = pd.merge(df, mapping,how="left", 
                  left_on=["camera", "rotaletter"],
                 right_on=["Channel", "Letter"])

    # 7. get required columns
    df_new = df_new[["date", "hourmin", "Rotameter Tag", "Excel_Num", "Excel_Tag", "bob_value"]]
    

    # 8. set sequence of sheets via a list
    list_of_rotas = ['Line 3_Carrier O2',
                    'Line 3_Header O2',
                    'Line 3_Sec O2',
                    'Line 3_Mix O2',
                    'Line 3_Mix LPG',
                    'Line 3_Fan LPG',
                    'Line 4_Carrier O2',
                    'Line 4_Slit O2',
                    'Line 4_NG',
                    ]

    # 8. output xlsx file
    save_xls(df_new, 
         list_of_rotas,
         #mapping["Rotameter Tag"].unique().tolist(),
        outputfilepath)

    return


if __name__ == '__main__': 

    PROCESSED_LOGS_FOLDER = os.path.join(os.getcwd(), 'logging')
    RESULTS_FOLDER = os.path.join(os.getcwd(), "results")

    # mapping table init
    mappingfile = "mapping.csv"
    MAPPING_TABLE_FOLDER = os.path.join(os.getcwd(), 'mapping')
    mapping = pd.read_csv(os.path.join(MAPPING_TABLE_FOLDER, mappingfile))

    ap = argparse.ArgumentParser()
    ap.add_argument("-ld", "--log_directory", default=PROCESSED_LOGS_FOLDER,
        required=False, help="directory path for log csv to be added.")
    
    ap.add_argument("-ofn", "--outputfilename", default="results.xlsx",
        required=False, help="directory path for log csv to be added.")

    args = vars(ap.parse_args())
    PROCESSED_LOGS_FOLDER = os.path.join(os.getcwd(), 'logging')
    logdir = args.get("log_directory", PROCESSED_LOGS_FOLDER)
    outputfilename = args.get("outputfilename", "results.xlsx")
    outputfilepath = os.path.join(RESULTS_FOLDER, outputfilename)
    df = consolidate_csvs(logdir)

    #TODO: ADD mapping table for rota type
    outputpivottable(df, outputfilepath, mapping)
    copyfile(outputfilepath, os.path.join(RESULTS_FOLDER, "results.xlsx"))
    copyfile(outputfilepath, os.path.join(RESULTS_FOLDER, "networkdrive","results.xlsx"))
