from flask import Flask
from config import Config
import subprocess
import os

app = Flask(__name__)
app.config.from_object(Config)

# start scheduler as well
process = subprocess.Popen(["python3", "run_schedule.py"])
os.environ['SCHEDULER_PID'] = str(process.pid) # set id to env

from ui import routes