# flasks
from flask import render_template, flash, redirect, url_for, request, jsonify, make_response
from ui import app

# web
from werkzeug.urls import url_parse
#from werkzeug import secure_filename

# ftp 
from ftplib import FTP
import ftputils as ftu 

# process image
import cv2
from imageproc import process_image
import tfutils as tu
import tensorflow as tf

# files
import json
import pandas as pd

# system
import os
import sys
import shutil
import logging

# ===== inits ===== 

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# folder names
APP_ROOT = os.path.dirname(os.path.abspath(__file__))   # refers to application_top
APP_STATIC = os.path.join(APP_ROOT, 'static')
RAW_IMAGE_FOLDER = os.path.join(APP_STATIC, 'assets', 'rawimage')
PROCESS_IMAGE_FOLDER = os.path.join(APP_STATIC, 'assets', 'processedimage')
MODEL_DIR = os.path.join(os.getcwd(), 'model')
MODEL_FILE_NAME = os.environ['MODELNAME']
MIXLPG_MODEL_FILENAME = os.environ['MIXLPGMODEL']
CH7AH_MODEL_FILENAME = os.environ['CH7AH']

# ftp vars
HOSTNAME = os.environ['FTPHOST'] 
FTPPORT = int(os.environ['FTPPORT'])
print(HOSTNAME, FTPPORT) 
FTPUSERNAME = os.environ['FTPUSERNAME'] 
FTPPASSWORD = os.environ['FTPPASSWORD'] 
print(FTPUSERNAME, FTPPASSWORD)
IMAGE_DIR = os.environ['FTPFOLDERPATH']
print("FTP Image folder: {}".format(IMAGE_DIR))
ftp = FTP()

# tensorflow inits
print("===== tensorflow inits =====")
# main model
graph, session, image_tensor,\
    detection_boxes, detection_scores, detection_classes,\
        num_detections = tu.loadtfsessionvars(MODEL_DIR, MODEL_FILE_NAME)
logger.info("tensorflow graph loaded.")

# mixLPGmodel
mixlpg_graph, mixlpg_session, mixlpg_image_tensor,\
    mixlpg_detection_boxes, mixlpg_detection_scores, mixlpg_detection_classes,\
        mixlpg_num_detections = tu.loadtfsessionvars(MODEL_DIR, MIXLPG_MODEL_FILENAME)
logger.info("mixlpg tf graph loaded.")

# ch7ah model
ch7ah_graph, ch7ah_session, ch7ah_image_tensor,\
    ch7ah_detection_boxes, ch7ah_detection_scores, ch7ah_detection_classes,\
        ch7ah_num_detections = tu.loadtfsessionvars(MODEL_DIR, CH7AH_MODEL_FILENAME)
logger.info("ch7 A-H tf graph loaded.")


# mapping table inits
mappingfile = "mapping.csv"
mappingfolder = os.path.join(os.getcwd(), 'mapping')
mapping = pd.read_csv(os.path.join(mappingfolder, mappingfile))
print("===== mapping table =====")
print(mapping.head(3))

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():  
    
    hostname = request.form.get("hostname", HOSTNAME) 
    port = int(request.form.get("port", FTPPORT)) 
    username = request.form.get("username", FTPUSERNAME) 
    password = request.form.get("password", FTPPASSWORD) 
    ftpfolderpath = request.form.get("folderpath", IMAGE_DIR)
    modelfile = request.form.get("modelfile", MODEL_FILE_NAME)

    modelfiles = [f for f in os.listdir(MODEL_DIR) if f[-3:]==".pb"]
    
    # get directory contents from ftp
    dir_list = ftu.try_list_folder_contents(ftp,
            hostname, port, username, password, ftpfolderpath)
    return render_template("index.html",
            title="Denka Rotameter Monitoring Platform",
                                dir=dir_list,
                                hostname=hostname, 
                                port=port, 
                                username=username, 
                                password=password, 
                                folderpath=ftpfolderpath,
                                modelfile=modelfile,
                                modelfiles=modelfiles)

# process image
@app.route('/process', methods=['GET','POST']) 
def process(): 

    global MODEL_FILE_NAME, graph, session, image_tensor,\
            detection_boxes, detection_scores, detection_classes,\
                num_detections

    # handle null file 
    if request.form['filename'] == "" or request.form['filename'] is None:
        return jsonify({'image': request.form['filename'], 
                        'resultstring': "Error on Image Processing." })
    
    # handle bad modelfile name
    elif request.form["modelfile"][-3:] != ".pb":

        print(request.form["modelfile"])

        return jsonify({'image': "", 
                        'resultstring': "Bad Model file extension." })

    else:
        # 0 reset model if not same as loaded
        if MODEL_FILE_NAME != request.form["modelfile"]:
            MODEL_FILE_NAME = request.form["modelfile"]
            logger.info("Changing model to: {}".format(MODEL_FILE_NAME))
            #print("os env: {}".format(os.environ['MODELNAME']))
            try:
                graph, session, image_tensor,\
                detection_boxes, detection_scores, detection_classes,\
                    num_detections = tu.loadtfsessionvars(MODEL_DIR, MODEL_FILE_NAME)
            except Exception as e:
                logger.info("Cannot load TF model")
                return jsonify({'image': "", 
                        'resultstring': e })

        # 1. get image name from file 
        imagename = request.form['filename'] 
        folder_path = request.form['folderpath'] 
        if "/" in folder_path: 
            folder_path = folder_path.split("/")[-1]
        # 2. remove previous temp folder 
        shutil.rmtree(PROCESS_IMAGE_FOLDER) 
        os.mkdir(PROCESS_IMAGE_FOLDER)
        # 3. download image to temp folder
        original_image = ftu.load_ftp_img(
                    ftp, imagename, tempfolder=PROCESS_IMAGE_FOLDER, 
                    persist=True)
        
        # 4. process image
        processedimg, csvrow = process_image(original_image, imagename, mapping,
            steps={"narrow_width_view": 
                        {"x_width": 0.5, "x_center": 0.5},
                    "get_rotameter_id_and_name": {"x0":0, "y0":0, "x1":100, "y1":100},
                    "get_model_results": (session, image_tensor,  detection_boxes, 
                    detection_scores, detection_classes, num_detections),
                    "mixlpg_results": (mixlpg_session, mixlpg_image_tensor, mixlpg_detection_boxes, 
                    mixlpg_detection_scores, mixlpg_detection_classes, mixlpg_num_detections),
                    "ch7ah_results": (ch7ah_session, ch7ah_image_tensor, ch7ah_detection_boxes, 
                    ch7ah_detection_scores, ch7ah_detection_classes, ch7ah_num_detections),
                    "get_best_box" : 0.05,
                    "draw_box": True
                    }          
                                )

        # print(csvrow)

        # 5. write image to tmp preprocessed image folder. 
        cv2.imwrite(os.path.join(PROCESS_IMAGE_FOLDER, imagename),  
                    processedimg) 

        resultstring = """Model File: {}
            Camera: {}, Letter: {},
            High Px: {} (conf: {:0.2f}),   Low Px: {} (conf: {:0.2f}),
            Bob Px: {} (conf: {:0.2f}), Bob Value: {}""".format(
                MODEL_FILE_NAME,
                csvrow[2], csvrow[3], 
                csvrow[4], csvrow[5], csvrow[7], csvrow[8], 
                csvrow[10], csvrow[11], csvrow[12]
            )
    return jsonify({'image': imagename, 
                        'resultstring': resultstring })

@app.route('/displayimage', methods=['GET','POST']) 
def displayimage(): 
    # handle null file 
    if request.form['filename'] == "" or request.form['filename'] is None:
        return jsonify({'image': request.form['filename'], 
                        'resultstring': "Fail" })
     
    # === process file === 
    else: 
        # 1. get image name from file 
        imagename = request.form['filename'] 
        folder_path = request.form['folderpath'] 
        if "/" in folder_path: 
            folder_path = folder_path.split("/")[-1]
        # 2. remove previous temp folder 
        shutil.rmtree(RAW_IMAGE_FOLDER) 
        os.mkdir(RAW_IMAGE_FOLDER) 


        # 3. download image to temp folder
        try:
            ftu.load_ftp_img(
                ftp, imagename, tempfolder=RAW_IMAGE_FOLDER, 
                persist=True)
        except Exception as e:

            print(e)
            hostname = request.form.get("hostname", HOSTNAME) 
            port = int(request.form.get("port", FTPPORT)) 
            username = request.form.get("username", FTPUSERNAME) 
            password = request.form.get("password", FTPPASSWORD) 
            ftpfolderpath = request.form.get("folderpath", IMAGE_DIR)

            ftpresult = ftu.ftp_connect(ftp, hostname, port,  
                user=username, password=password, basedir=ftpfolderpath)
            print("ftp reconnect status: {}".format(ftpresult))
            ftu.load_ftp_img(
                ftp, imagename, tempfolder=RAW_IMAGE_FOLDER, 
                persist=True)
            



    return jsonify({'image': imagename, 
                        'resultstring': "pass" })