import os 
import time 
import schedule 
import datetime
import argparse

foldernamelist = []

def run_log_adhoc_images(folderpath): 
    print("starting scrape routine...") 
    os.system("python3 log_images.py -sd {}".format(folderpath)) 
    print("end of scrape rountine.")

if __name__ == '__main__': 

    # 1. get env values
    ftpfolderpath = os.environ["FTPFOLDERPATH"]

    # 2. parse args
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--files", default=[], type=str,
        required=False, help="file names")

    args = vars(ap.parse_args())

    filenames = args.get("files", None)

    # 3. process files
    if not filenames:
        print("No files listed.")
    else:
        foldernamelist = filenames.split(",")
        print("List of folders to process: {}".format(foldernamelist))
        
        # run log_images for each folder name
        for f in foldernamelist:
            #print(os.path.join(ftpfolderpath, f))
            run_log_adhoc_images(os.path.join(ftpfolderpath, f))