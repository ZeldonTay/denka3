import ftplib 
import os 
import shutil 
import cv2 
 
 
def ftp_connect(ftp, host,port, user="", password="", basedir=".", timeoutpar=5): 
    """[summary] 
     
    Arguments: 
        ftp {ftplib obj} -- current ftplib object 
        host {str} -- host ip 
        port {int} -- port number 
     
    Keyword Arguments: 
        user {str} -- username (default: {""}) 
        password {str} -- password (default: {""}) 
        basedir {str} -- ftp base directory to cd to (default: {""}) 
     
    Returns: 
        {str} -- Returns a string to describe success/failure of connection. 
    """ 
    print("connecting to ftp...") 
    try: 
        print(ftp.connect(host, port, timeout=timeoutpar)) 
    except: 
        return "Error in connecting to host." 
     
    try: 
        print(ftp.login(user, password)) 
    except: 
        return "Error during login." 
 
    try: 
        print(ftp.cwd(basedir)) 
    except: 
        return "unable to switch to specified directory." 
 
    return "FTP login succeeded." 
 
 
def processftpfolder(ftp): 
    """ 
    Filter away folder or files that have '.' as a starting char. 
     
    Arguments: 
        ftp {ftplib object} -- the current ftp object. 
     
    Returns: 
        [list] -- list of filtered files/folders in directory. 
    """ 
    dirlist = ftp.nlst() 
    dirlist = [x for x in dirlist if x[0]!='.'] 
 
    return dirlist 
 
 
def load_ftp_img(ftp, filename, tempfolder="ftp_tmp", persist=False): 
    """ 
    Downloads file in temp folder and reads into numpy array with cv2.imread. 
     
    Arguments: 
        ftp {ftplib object} -- current ftplib object. 
        filename {str} -- name of file to download and load. 
     
    Keyword Arguments: 
        tempfolder {str} -- name of temp folder to hold downloaded file.  
        will be deleted at end of process if persist=False (default: {"ftp_tmp"}). 
     
    Returns: 
        img {np array} -- image in BGR channels as numpy array. 
    """ 
 
 
    #destination_dir = os.getcwd()+"/"+tempfolder 
     
    # create tmp folder if not exist 
    if not os.path.isdir(tempfolder): 
        os.mkdir(tempfolder) 
     
    #print("type of filename: ", type(filename)) 
    filepath = os.path.join(os.getcwd(), tempfolder, filename) 
     
    # download file from ftp site 
    with open(filepath, 'wb') as f: 
        ftp.retrbinary('RETR ' + filename, f.write, 1024) 
     
    # read image via cv2 
    try: 
        img = cv2.imread(filepath) 
    except: 
        img = "Error: file might not be image file." 
 
    # remove folder if persist == False 
    if not persist: 
        shutil.rmtree(tempfolder) 
     
    return img 


def try_list_folder_contents(ftp, hostname, port, 
    username, password, ftpfolderpath):

    # 1. If not even initialized, try login
    if ftp.getwelcome():
        try:
            ftpresult = ftp_connect(ftp, hostname, port,  
            user=username, password=password, basedir=ftpfolderpath)
            if ftpresult != "FTP login succeeded.":
                return [ftpresult]
        except Exception as e:
            errormsg = "Error in Login: {}".format(e)
            print(errormsg)
            return [e]
    
    # 2. Try cd to folderpath and process folder. 
    # 3. If fail, try reconnect
    try:
        ftp.cwd(ftpfolderpath)
        dir_list = processftpfolder(ftp)
    except Exception as e:
        print("Error in processing folder: {}".format(e))
        print("Re-logging in...")
        try:
            ftpresult = ftp_connect(ftp, hostname, port,  
            user=username, password=password, basedir=ftpfolderpath)
            dir_list = processftpfolder(ftp)
        except Exception as e:
            errormsg = "Error in Re-Login: {}".format(e)
            print(errormsg)
            return [errormsg]

    return dir_list