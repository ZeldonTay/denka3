import cv2
import numpy as np
import tensorflow as tf

import datetime
import pytesseract
import math

def get_rotameter_id_and_name(original, mappingtable, 
    x0=0, y0=0, x1=50, y1=60, showwindow=False, shift=False): 
    
    img = original.copy() 
    # # label coords 
    # x0 = 15 
    # y0 = 60 
    # x1 = 280 
    # y1 = 110 
 
    # get slice of image where label is at
    label_img = img[y0:y1, x0:x1].copy() 
    
    # shift letter slightly right
    if shift:
        # get black background
        label_img_black = label_img.copy()
        label_img_black[:]=(0,0,0)

        # slice part that we need to shift
        
        slicewidth = 48
        sliceheight = 48
        startOfshiftX = math.floor(((x1-x0)-slicewidth)/2)
        startOfshiftY=math.floor(((y1-y0)-sliceheight)/2)
        print("x0: {} x1: {}".format(x0, x1))
        print("start of shift: {}".format(startOfshiftX))

        # add slice back
        label_img_black[(y0+startOfshiftY):(y0+startOfshiftY+sliceheight),
                    (x0+startOfshiftX):(x0+startOfshiftX+slicewidth)]=\
            label_img[y0:(y0+sliceheight), x0:(x0+slicewidth)]
        
        # reassign label_img
        label_img= label_img_black
        


    # refine label image via grayscale 
    label_img_gray = cv2.cvtColor(label_img, cv2.COLOR_BGR2GRAY) 

    # below this tresh becomes black
    #ret, label_img_thresh = cv2.threshold(label_img_gray, 0, 219, 255)
    ret, label_img_thresh = cv2.threshold(label_img_gray, 215, 225, 0)
    

    #label_img_thresh=label_img_gray

    ocr_input_image = label_img_thresh 

    #config = ("-l eng --oem 1 --psm 7") 
    config = ("-c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ --oem 0 --psm 10") 

    try: 
        #letter = pytesseract.image_to_string(label_img_thresh,  
        letter = pytesseract.image_to_string(label_img_gray,
                    config=config) 
        print("OCR-ed Character: ", letter) 

        # TODO to find rotaname via mappingtable
        rotaname = "unknown"

    except: 
        print("Cannot identify Lettering for Rotameter Name.") 
        print("Trying old Rota Id function")  
        rotaname = "Unknown" 
        letter = "Unknown" 
    
    # letter proxy correction
    if letter=="Y": letter="I"
    return label_img_thresh, rotaname, letter 

def narrow_width_view(image, para={"x_width": 0.5, "x_center": 0.5}):
    #processedimg = image.copy()
    processedimg = image
    img_width = processedimg.shape[1]
    img_height = processedimg.shape[0]

    mask = np.zeros((img_height, img_width), dtype=np.uint8)
    
    maskoutwidth = img_width * para["x_width"]
    mask_center = img_width * para["x_center"]
    mask_x1 = int(max(mask_center - (maskoutwidth*0.5), 0))
    mask_x2 = int(min(mask_center + (maskoutwidth*0.5), img_width))
    
    points = np.array([[
            [mask_x1, 0], [mask_x2, 0], 
            [mask_x2, img_height], [mask_x1, img_height]
                        ]])
    
    cv2.fillPoly(mask, points, (255))

    processedimg = cv2.bitwise_and(processedimg,processedimg,mask = mask)
    return processedimg

def get_model_results(img, session, image_tensor,  detection_boxes, 
                    detection_scores, detection_classes, num_detections):

    # convert image to RGB because yan's 4_yolo_covert uses Pil,
    # while his reads use cv3
    #img = img.copy()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    image_np_expanded = np.expand_dims(img, axis=0) 
    (boxes, scores, classes, num) = session.run( 
    [detection_boxes,detection_scores, detection_classes, num_detections], 
    feed_dict={image_tensor: image_np_expanded})

    blueindices = np.where(classes==1)
    bluescores = scores[blueindices]
    if bluescores.size == 0: bluescores = np.zeros(1)
    blueboxes = boxes[blueindices]
    if blueboxes.size == 0: blueboxes = np.zeros(1)
    blues = {"scores": bluescores, 
            "boxes": blueboxes}

    yellowindices = np.where(classes==2)
    yellowscores = scores[yellowindices]
    if yellowscores.size == 0: yellowscores = np.zeros(1)
    yellowboxes = boxes[yellowindices]
    if yellowboxes.size == 0: yellowboxes = np.zeros(1)
    yellows = {"scores": yellowscores,
               "boxes": yellowboxes}

    bobindices = np.where(classes==3)
    bobscores = scores[bobindices]
    if bobscores.size == 0: bobscores = np.zeros(1)
    bobboxes = boxes[bobindices]
    if bobboxes.size == 0: bobboxes = np.zeros(1)
    bobs = {"scores": bobscores, 
            "boxes": bobboxes}
            
    return (blues, yellows, bobs)

def get_best_box(img, blues, yellows, bobs, per=0.05):
    img_width = img.shape[1]
    img_height = img.shape[0]

    # get stats if box prob greater than per
    if blues["scores"][0] > per:
        x1 = blues["boxes"][0][1]*img_width
        y1 = blues["boxes"][0][0]*img_height
        x2 = blues["boxes"][0][3]*img_width
        y2 = blues["boxes"][0][2]*img_height
        ymid = abs(y2+y1)/2

        blue = {"score": blues["scores"][0],
                "box": blues["boxes"][0],
                "xys": [int(x1), int(y1), int(x2), int(y2)],
                "y-level": int(ymid)}
    else:
        blue = {"score": 0, "box": [0,0,0,0], 
                "xys": [0,0,0,0], "y-level": 0}
    
    if yellows["scores"][0] > per:
        x1 = yellows["boxes"][0][1]*img_width
        y1 = yellows["boxes"][0][0]*img_height
        x2 = yellows["boxes"][0][3]*img_width
        y2 = yellows["boxes"][0][2]*img_height
        ymid = abs(y2+y1)/2

        yellow = {"score": yellows["scores"][0],
                "box": yellows["boxes"][0],
                "xys": [int(x1), int(y1), int(x2), int(y2)],
                "y-level": int(ymid)}
    else:
        yellow = {"score": 0, "box": [0,0,0,0], 
                  "xys": [0,0,0,0], "y-level": 0}
    
    if bobs["scores"][0] > per:
        x1 = bobs["boxes"][0][1]*img_width
        y1 = bobs["boxes"][0][0]*img_height
        x2 = bobs["boxes"][0][3]*img_width
        y2 = bobs["boxes"][0][2]*img_height
        bob = {"score": bobs["scores"][0],
                "box": bobs["boxes"][0],
                "xys": [int(x1), int(y1), int(x2), int(y2)],
                "y-level": int(y1)}
    else:
        bob = {"score": 0, "box": [0,0,0,0],
               "xys": [0,0,0,0], "y-level": 0}


    return blue, yellow, bob

def draw_box(img, blue, yellow, bob, drawcenter=True):
    img_width = img.shape[1]
    img_height = img.shape[0]

    if blue["score"] > 0:
        img = cv2.rectangle(img, 
            (blue["xys"][0], blue["xys"][1]), 
            (blue["xys"][2], blue["xys"][3]), 
            (255,0,0), thickness=2
        )

        if drawcenter:
            img = cv2.line(img,  
                (0, blue["y-level"]), (img_width, blue["y-level"]),  
                (255,0,0), thickness=2)
    
    if yellow["score"] > 0:
        img = cv2.rectangle(img, 
            (yellow["xys"][0], yellow["xys"][1]), 
            (yellow["xys"][2], yellow["xys"][3]), 
            (0,255,255), thickness=2
        )

        if drawcenter:
            img = cv2.line(img,  
                (0, yellow["y-level"]), (img_width, yellow["y-level"]),  
                (0,255,255), thickness=2)
    
    if bob["score"] > 0:
        img = cv2.rectangle(img, 
            (bob["xys"][0], bob["xys"][1]), 
            (bob["xys"][2], bob["xys"][3]), 
            (0,255,0), thickness=2
        )

        if drawcenter:
            img = cv2.line(img,  
                (0, bob["y-level"]), (img_width, bob["y-level"]),  
                (0,255,0), thickness=2)
    
    return img

def calculate_bob_value(mapping, camera, rotaletter, 
        high_sticker_px, low_sticker_px, bob_px):
    
    # filter for specific rotameter dataframe
    rotadf = mapping[(mapping["Channel"] == camera
            ) & (mapping["Letter"] == rotaletter)]

    # get high sticker value
    try:
        highvalue = rotadf["High"].values[0]
    except:
        highvalue = 0
    # get low sticker value
    try:
        lowvalue = rotadf["Low"].values[0]
    except:
        lowvalue = 0
    
    print("highvalue: {}, lowvalue: {}".format(highvalue, lowvalue))

    # get rota line type
    try:
        rotatype = rotadf["Rotameter Tag"].values[0]
    except:
        rotatype = None

    # calculate bob value
    if (low_sticker_px - highvalue)!= 0:

        if rotatype == "Line 4_Slit O2":
            try:
                bob_inter_value = round(abs(118 - 0)*(low_sticker_px - bob_px)/abs(
                                    low_sticker_px - high_sticker_px) + 0, 3)
                
                bobvalue = -0.00000000506710948273609*(bob_inter_value**3) + (0.0000297213360943333*(bob_inter_value**2))\
                    + (0.0219871656*bob_inter_value) + 1
            
            except Exception as e:
                print(e)
                bobvalue = 0
            

        else:
            try:
                bobvalue = round(abs(highvalue - lowvalue)*(low_sticker_px - bob_px)/abs(
                                low_sticker_px - high_sticker_px) + lowvalue, 3)
            except Exception as e:
                print(e)
                bobvalue = 0
    else:
        return "unknown"
    
    # filter for ridiculous bobvalues
    if bobvalue<0:
        return "unknown"
    elif bobvalue>50:
        return "unknown"
    else:
        return bobvalue

def determine_high_low(blue, yellow):
    if yellow["y-level"] < blue["y-level"]:
        high_color = "yellow"
        high_sticker_px = yellow["y-level"]
        high_confidence = yellow["score"]
        low_color = "blue"
        low_sticker_px = blue["y-level"]
        low_confidence = blue["score"]
    else:
        high_color = "blue"
        high_sticker_px = blue["y-level"]
        high_confidence = blue["score"]
        low_color = "yellow"
        low_sticker_px = yellow["y-level"]
        low_confidence = yellow["score"]
    
    return (high_color, high_sticker_px, high_confidence,
            low_color, low_sticker_px, low_confidence)

def process_image(image, imagename, mapping,
    steps={"narrow_width_view": {"x_width": 0.5, "x_center": 0.5},
            "get_best_box": 0.05,
            }):
    """
    Available steps:
        "narrow_width_view": {"x_width": 0.5, "x_center": 0.5},
        "get_rotameter_id_and_name": {"x0":0, "y0":0, "x1":110, "y1":60},
        "get_model_results": (session, image_tensor,  detection_boxes, 
                    detection_scores, detection_classes, num_detections),
        "get_best_box" : 0.05,
        "draw_box": True
    """
    #inits
    
    blues = {"scores": [0], "boxes": [[0,0,0,0]]}
    yellows = {"scores": [0], "boxes": [[0,0,0,0]]}
    bobs = {"scores": [0], "boxes": [[0,0,0,0]]}

    blue = {"score": 0, "box": [0,0,0,0], "xys": [0,0,0,0], "y-level": 0}
    yellow = {"score": 0, "box": [0,0,0,0], "xys": [0,0,0,0], "y-level": 0}
    bob = {"score": 0, "box": [0,0,0,0], "xys": [0,0,0,0], "y-level": 0}

    processedimg = image.copy()
    camera = imagename.split("_")[1]
    time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    rotaletter = "unknown"
    csvrow = [time, "unknown", "unknown", 
                "unknown", "unknown", "unknown",
                "unknown", "unknown", "unknown",
                "unknown", "unknown", "unknown"]

    if "narrow_width_view" in steps:
        try:
            processedimg = narrow_width_view(processedimg,
                para=steps["narrow_width_view"]
            )
        except Exception as e:
            print("Error on Masking Operation: {}".format(e))
    
    if "get_rotameter_id_and_name" in steps:
        rotaid_image, rotaname, rotaletter = get_rotameter_id_and_name(image.copy(), mapping, **steps["get_rotameter_id_and_name"])
        stacked_img = np.stack((rotaid_image,)*3, axis=-1)
        # overlay letter back to image
        processedimg[steps["get_rotameter_id_and_name"]["y0"]:steps["get_rotameter_id_and_name"]["y1"],
                        steps["get_rotameter_id_and_name"]["x0"]:steps["get_rotameter_id_and_name"]["x1"]] = stacked_img # image[0:88,0:110]

        # expose top left rota name
        #processedimg[0:60,150:420] = image[0:60,150:420]

        # expose bottom right rota details
        #processedimg[1010:1080,0:260] = image[1010:1080,0:260]

        # diagnostic draw letter inference box
        processedimg = cv2.rectangle(processedimg, (0,0), (100,100), (0,255,0), 2)

    if "get_model_results" in steps:
        try:
            # a. get rota channel
            rotachannel = imagename[4:8]
            rotachanId = rotachannel + rotaletter

            # b. route to necessary rota model
            if (rotachanId == "ch18F") or (rotachanId == "ch18I") or (rotachanId == "ch18L"):
                blues, yellows, bobs = get_model_results(processedimg, 
                *steps["mixlpg_results"])
                print("used mixlpg model.")
            elif (imagename[4:7] + rotaletter) in ["ch7C", "ch7D", "ch7E", "ch7F", "ch7G", "ch7H"] :
                blues, yellows, bobs = get_model_results(processedimg, 
                *steps["ch7ah_results"])
                print("used ch7ah model.")
            else:
                blues, yellows, bobs = get_model_results(processedimg, 
                *steps["get_model_results"])

        except Exception as e:
            print("Error on Image Process with TF Model: {}".format(e))
    
    if "get_best_box" in steps:
        try:
            blue, yellow, bob = get_best_box(processedimg,
                                blues, yellows, bobs, 
                                per=steps["get_best_box"])
            
            print("best results: ")
            print("blue: {}".format(blue))
            print("yellow: {}".format(yellow))
            print("bob: {}".format(bob))

        except Exception as e:
            print("Error on retrieving best boxes: {}".format(e))

    if "draw_box" in steps:
        try:
            processedimg = draw_box(processedimg, blue, yellow, bob, 
                            drawcenter=steps["draw_box"])
        except Exception as e:
            print("Error on drawing boxes: {}".format(e))

    # ===== write out result stats ===== #
    # time, camera, rotaletter,
    # low_sticker_px, low_confidence, low_color, 
    # high_sticker_px, high_confidence, high_color, 
    # bob_px, bob_confidence, bob_value

    # determine high/low
    high_color, high_sticker_px, high_confidence,\
        low_color, low_sticker_px, low_confidence = determine_high_low(blue,yellow)

    # get bob stats
    bob_px = bob["y-level"]
    bob_confidence = bob["score"]

    # determine bob_value
    print(camera)
    print(rotaletter)

    if (high_confidence<=steps["get_best_box"]) or\
         (low_confidence<=steps["get_best_box"]) or\
             (bob_confidence<=steps["get_best_box"]):
        bob_value = "unknown"
    else:
        bob_value = calculate_bob_value(mapping, camera, rotaletter, 
            high_sticker_px, low_sticker_px, bob_px)

    # form csvrow
    csvrow = [time, imagename, camera, rotaletter, 
                high_sticker_px, high_confidence, high_color,
                low_sticker_px, low_confidence, low_color,
                bob_px, bob_confidence, bob_value]
    
    print("csvrow: ")
    print(csvrow)
    return processedimg, csvrow