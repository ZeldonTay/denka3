import os 
import time 
import schedule 
import datetime

def run_log_images(folderpath): 
    print("starting scrape routine...") 
    os.system("python3 log_images.py -sd {}".format(
        os.path.join(folderpath, 
            datetime.datetime.today().strftime('%Y-%m-%d')
            )
    )) 
    print("end of scrape rountine at {}".format(
        datetime.datetime.today().strftime('%Y-%m-%d %H:%M')))
    
    print("run consolidate results...")
    os.system("python3 consolidatecsv.py -ofn {}".format(
        "results_"+datetime.datetime.today().strftime('%Y-%m-%d_%H%M')+".xlsx"))
    print("end of consolidation at {}".format(
        datetime.datetime.today().strftime('%Y-%m-%d %H%M')+"hrs"
    ))


if __name__ == '__main__': 
    
    print("Current time is: {}".format(datetime.datetime.now()))

    ftpfolderpath = os.environ['FTPFOLDERPATH']
    # 1. parse schedules
    intervals = os.environ['SCHEDULES'] 
    intervals = intervals.split("|") 
    print("running scheduler at times: ", intervals) 
    
    # 2. Add schedules daily to schedule module
    for t in intervals: 
        timing = datetime.datetime.strptime(t, '%H:%M')# - datetime.timedelta(hours=8) 
        timing = timing.strftime("%H:%M") 
        #print(timing) 
        schedule.every().day.at(timing).do(run_log_images, ftpfolderpath) 
        #schedule.every().day.at(timing).do(run_log_images) 
    
    # 3. While true, run pending schedules for the day
    while True: 
        schedule.run_pending() 
        time.sleep(120) # wait one minute 
        print("scheduler is running...") 

