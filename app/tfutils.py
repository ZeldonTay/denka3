import os 
import tensorflow as tf 
import logging

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def load_graph(frozen_graph_path, debug=False):
    if debug: print ("DEBUG: Loading graph...") 
    with tf.gfile.GFile(frozen_graph_path, "rb") as f: 
        graph_def = tf.GraphDef() 
        graph_def.ParseFromString(f.read()) 
    with tf.Graph().as_default() as graph: # care to pick correct graph session
        tf.import_graph_def(graph_def, name="prefix") 
    if debug: ("DEBUG: Graph loaded") 
    return graph 
 
def loadtfsessionvars(model_dir, frozenmodelname, debug=False): 
    
    graph = load_graph(os.path.join(model_dir,frozenmodelname))
    logger.info("Model: {} loaded.".format(frozenmodelname))
    session = tf.Session(graph=graph)
    image_tensor = graph.get_tensor_by_name('prefix/image_tensor:0')
    detection_boxes = graph.get_tensor_by_name('prefix/detection_boxes:0')
    detection_scores = graph.get_tensor_by_name('prefix/detection_scores:0')
    detection_classes = graph.get_tensor_by_name('prefix/detection_classes:0')
    num_detections = graph.get_tensor_by_name('prefix/num_detections:0')
    
    return graph,  session, image_tensor,  detection_boxes,\
         detection_scores, detection_classes, num_detections 