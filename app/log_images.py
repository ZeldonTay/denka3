# ftp 
from ftplib import FTP
import ftputils as ftu 
# process image
import cv2
from imageproc import process_image
import tfutils as tu
import tensorflow as tf
# system
import os
import sys
import shutil
import logging
import argparse
import datetime

# tables
import csv
import pandas as pd

def outputcsv(row=["",""], path="boblogs.csv", 
    header=["time", "imagename", "camera", "rotaletter", 
                "high_sticker_px", "high_confidence", "high_color",
                "low_sticker_px", "low_confidence", "low_color",
                "bob_px", "bob_confidence", "bob_value"]): 
    if os.path.isfile(path): 
        with open(path, 'a') as csv_file: 
            writer = csv.writer(csv_file) 
            writer.writerow(row) 
    else: 
        with open(path, 'w') as csv_file:
            writer = csv.writer(csv_file) 

            # 1. write header
            writer.writerow(header) 

            # 2. write filler rows for pivottable placeholder
            time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
            datestring = datetime.datetime.now().strftime('%Y%m%d')
            intervals = os.environ['SCHEDULES'] 
            intervals = intervals.split("|")
            letters = ["8A", "8B", "8C", "8D", "8E", "8F", "8G", "8H", "8I"]
            rowslist = [[time, "8_8_"+datestring+t[:2]+"00_T.jpg", "ch88", l, 88, 1, "blue", 88, 1, "yellow", 88, 1, 8] for t in intervals for l in letters]
            for r in rowslist:
                writer.writerow(r)

            # 3. write row
            writer.writerow(row) 
 
    csv_file.close() 
    return row

if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    logger.info("Initialize Mass Image Logging...")
    HOSTNAME = os.environ['FTPHOST'] 
    print(HOSTNAME) 
    FTPPORT = int(os.environ['FTPPORT']) 
    print(FTPPORT) 
    FTPUSERNAME = os.environ['FTPUSERNAME'] 
    print(FTPUSERNAME) 
    FTPPASSWORD = os.environ['FTPPASSWORD'] 
    print(FTPPASSWORD)
    IMAGE_DIR = os.environ['FTPFOLDERPATH']
    print(IMAGE_DIR)
    ftp = FTP()

    MODEL_DIR = os.path.join(os.getcwd(), 'model')
    MODEL_FILE_NAME = os.environ['MODELNAME']
    MIXLPG_MODEL_FILENAME = os.environ['MIXLPGMODEL']
    CH7AH_MODEL_FILENAME = os.environ['CH7AH']
    PROCESSED_LOGS_FOLDER = os.path.join(os.getcwd(), 'logging')
    PROCESS_IMAGE_FOLDER = os.path.join(PROCESSED_LOGS_FOLDER, 'tmp')
    SAVED_PROCESSED_IMAGES_FOLDER = os.path.join(PROCESSED_LOGS_FOLDER, 'savedimages')

    # create saved images folder if not exist
    if not os.path.isdir(SAVED_PROCESSED_IMAGES_FOLDER): 
        os.mkdir(SAVED_PROCESSED_IMAGES_FOLDER)

    print("AI model folder: {}".format(MODEL_DIR))
    logger.info("AI model name: {}".format(MODEL_FILE_NAME))
    graph, session, image_tensor,\
        detection_boxes, detection_scores, detection_classes,\
            num_detections = tu.loadtfsessionvars(MODEL_DIR, MODEL_FILE_NAME)
    logger.info("tensorflow graph loaded.")

    # mixLPGmodel
    mixlpg_graph, mixlpg_session, mixlpg_image_tensor,\
        mixlpg_detection_boxes, mixlpg_detection_scores, mixlpg_detection_classes,\
            mixlpg_num_detections = tu.loadtfsessionvars(MODEL_DIR, MIXLPG_MODEL_FILENAME)
    logger.info("mixlpg tf graph loaded.")

    # ch7ah model
    ch7ah_graph, ch7ah_session, ch7ah_image_tensor,\
        ch7ah_detection_boxes, ch7ah_detection_scores, ch7ah_detection_classes,\
            ch7ah_num_detections = tu.loadtfsessionvars(MODEL_DIR, CH7AH_MODEL_FILENAME)
    logger.info("ch7 A-H tf graph loaded.")

    # mapping table init
    mappingfile = "mapping.csv"
    mappingfolder = os.path.join(os.getcwd(), 'mapping')
    mapping = pd.read_csv(os.path.join(mappingfolder, mappingfile))
    # check if there is null values
    print("mapping table: ")
    print(mapping.head())
    print(mapping.isna().any())

    ap = argparse.ArgumentParser()
    ap.add_argument("-hn", "--hostname", default=HOSTNAME,
        required=False, help="host destination ip or name")
    
    ap.add_argument("-po", "--port", default=FTPPORT,
        required=False, help="port num of ftp server")
    
    ap.add_argument("-un", "--username", default=FTPUSERNAME,
        required=False, help="Username of loggin.")

    ap.add_argument("-pw", "--password", default=FTPPASSWORD,
        required=False, help="password of host ftp server")
    
    ap.add_argument("-sd", "--source_directory", default=IMAGE_DIR,
        required=False, help="directory path where images are to be scraped")

    ap.add_argument("-ld", "--log_directory", default=PROCESSED_LOGS_FOLDER,
        required=False, help="directory path for log csv to be added.")

    ap.add_argument("-simgf", "--save_image_flag", default=False, type=bool,
        required=False, help="directory path for log csv to be added.")

    args = vars(ap.parse_args())

    hostname = args.get("hostname", HOSTNAME)
    port = args.get("port", FTPPORT)
    username = args.get("username", FTPUSERNAME)
    password = args.get("password", FTPPASSWORD)
    ftpfolderpath = args.get("source_directory", IMAGE_DIR)
    logdir = args.get("log_directory", PROCESSED_LOGS_FOLDER)
    save_img_flag = args.get("save_image_flag", False)

    outputlogpath = os.path.join(logdir, ftpfolderpath.split("/")[-1]+".csv")
    print("Output Log Name: {}".format(outputlogpath))

    # check if there are existing records
    existing_images_list = []
    try:
        existingdf = pd.read_csv(outputlogpath)
        existing_images_list = existingdf["imagename"].values
        #print(existing_images_list)
    except Exception as e:
        print("Did not read existing log file.")
        logger.info(e)

    # connect to ftp
    ftpresult = ftu.ftp_connect(ftp, hostname, port,  
        user=username, password=password, basedir=ftpfolderpath)
    
    if ftpresult == "FTP login succeeded.":
            dir_list = ftu.processftpfolder(ftp)

            for imagename in dir_list:
                # check for pre-requisites
                        # if extension is correct
                        # never processed before
                if imagename not in existing_images_list:
                    logger.info("processing image: {}".format(imagename))
                    # then process image
                    try:
                        shutil.rmtree(PROCESS_IMAGE_FOLDER)
                    except:
                        pass

                    original_image = ftu.load_ftp_img(
                        ftp, imagename, tempfolder=PROCESS_IMAGE_FOLDER, 
                        persist=True)

                    processedimg, csvrow = process_image(original_image, imagename, mapping,
                        steps={"narrow_width_view": 
                                    {"x_width": 0.5, "x_center": 0.5},
                                "get_rotameter_id_and_name": {"x0":0, "y0":0, "x1":100, "y1":100},
                                "get_model_results": (session, image_tensor,  detection_boxes, 
                                detection_scores, detection_classes, num_detections),
                                "mixlpg_results": (mixlpg_session, mixlpg_image_tensor, mixlpg_detection_boxes, 
                    mixlpg_detection_scores, mixlpg_detection_classes, mixlpg_num_detections),
                                "ch7ah_results": (ch7ah_session, ch7ah_image_tensor, ch7ah_detection_boxes, 
                                    ch7ah_detection_scores, ch7ah_detection_classes, ch7ah_num_detections),
                                "get_best_box" : 0.05,
                                "draw_box": True
                                }
                                            
                                            )

                    # output csv              
                    outputcsv(row=csvrow, path=outputlogpath)

                    if save_img_flag:
                        cv2.imwrite(os.path.join(
                            SAVED_PROCESSED_IMAGES_FOLDER, imagename),
                            processedimg
                            )
                
                else:
                    logger.info("{} already processed".format(imagename))
            

    
    else:
        print("Unable to loggin to FTP server.")
    

