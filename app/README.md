## Some Notes

### Adhoc Run

#### 1. Run adhoc process to csv log
- file: **log_images.py**
- -hn or --hostname, default=$FTPHOST=192.168.128.250
- -po or --port, default=$FTPPORT=21
- -un or --username
- -pw or --password
- -sd or --source_directory, default=$FTPFOLDERPATH=/DATA/192.168.128.30
- -ld or --log_directory, default=getcwd + "logging"
- -simgf or --save_image_flag

#### 2. Consolidate All CSV and Output the Results.xlsx file  

 - file: **consolidatecsv.py** 
  
 - -ld or --log_directory will look for the log directory folder path.
  (this path defaults to /app/logging in app.)

 -  -ofn or --outputfilename will default to "results.xlsx"
  
 -  (However, will also output extra copy to /app/networkdrive/results.xlsx)

---
### Scheduled Run
- Is invoked in __init__.py in app folder.
- Will run at intervals specified in docker-compose env vars for $SCHEDULES.
- Sleeps for every 2 minutes
- Start: 
  1. Runs **log_images.py** -sd {FTPFOLDERPATH (normally /DATA/192.168.128.30)}+"/%Y-%m-%d"
  2. Then runs **consolidate.py** -ofn {results_%Y-%m-%d_%H:%M.xlsx}

---

### Folder Structure

.
├── app
│   ├── logging
│   ├── mapping -> to map rotameter to lettering
│   │   └── mapping.csv
│   ├── model
│   ├── results
│   ├── ui
│   │   ├── __init__.py **-> runs: subprocess.Popen(["python3", "run_schedule.py"])**
│   │   ├── routes.py -> api routing
│   │   ├── static
│   │   └── templates
│   └── ui_start.sh - for starting ui locally


│   ├── clientflask.py
│   ├── config.py
│   ├── eng.traineddata - pytesseract model file
│   ├── Dockerfile


│   ├── imageproc.py **-> library for processing images**


│   ├── log_images.py **-> 1**
│   ├── consolidatecsv.py **-> 2**
│   ├── run_schedule.py **-> runs: log_images.py then consolidatecsv.py**
│   ├── run_adhoc.py **-> runs: log_images only**

│   ├── ftputils.py
│   ├── tfutils.py
│   ├── README.md
├── docker-compose.yml
└── ftpserver - to mimic ftpserver when needed